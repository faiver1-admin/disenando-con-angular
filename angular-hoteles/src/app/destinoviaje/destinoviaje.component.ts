import { Component,Input, OnInit, HostBinding } from '@angular/core';
import { DestinoViaje} from './../models/destinoviaje.model';

@Component({
  selector: 'app-destinoviaje',
  templateUrl: './destinoviaje.component.html',
  styleUrls: ['./destinoviaje.component.css']
})
export class DestinoviajeComponent implements OnInit {
  @Input  destinos: DestinoViaje;
  @HostBinding('attr.class') cssClass='col-md-4';

  constructor() { }

  ngOnInit(){
  }

}
